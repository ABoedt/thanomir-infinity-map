package com.example.busittestproject;

import android.content.Context;

public class ResourceUtility {

    public static int getResourceDrawableID(Context context, String resourceName){
        return context.getResources().getIdentifier(resourceName, "drawable", context.getPackageName());
    }

}
