package com.example.busittestproject;

public class EndBoss {
    private static boolean dead;
    private static boolean secondChance = false;

    public static boolean isSecondChance() {
        return secondChance;
    }

    public static void setSecondChance(boolean secondChance) {
        EndBoss.secondChance = secondChance;
    }

    public static void bossDied() {
        dead = true;
    }


    public static boolean isDead() {
        return dead;
    }
}
