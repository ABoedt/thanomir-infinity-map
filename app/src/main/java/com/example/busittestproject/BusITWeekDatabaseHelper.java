package com.example.busittestproject;

import cz.mendelu.busItWeek.library.StoryLineDatabaseHelper;
import cz.mendelu.busItWeek.library.builder.StoryLineBuilder;

public class BusITWeekDatabaseHelper extends StoryLineDatabaseHelper {

    public BusITWeekDatabaseHelper() {
        super(89);
    }

    @Override
    protected void onCreate(StoryLineBuilder builder) {


        builder.addGPSTask("Red Skull")
                .location(49.209984, 16.614801)
                .radius(10)
                .victoryPoints(10)
                .hint("It's blue and shiny")
                .simplePuzzle()
                    .hint("redskull")
                    .puzzleTime(30000)
                    .question("What was the name of the object I used?")
                    .answer("tesseract")
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Grandmaster")
                .location(49.210456, 16.615160)
                .radius(10)
                .victoryPoints(10)
                .hint("It's green and angry :^)")
                .imageSelectPuzzle()
                    .hint("grandmaster")
                    .puzzleTime(3000)
                    .question("Who’s my greatest champion?")
                    .addImage(R.drawable.hulk, true)
                    .addImage(R.drawable.korg, false)
                    .addImage(R.drawable.skurge, false)
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Vulture")
                .location(49.209937, 16.615691)
                .radius(15)
                .victoryPoints(10)
                .hint("Is it a bird, is it a plane no it's vulture")
                .choicePuzzle()
                    .hint("vulture")
                    .puzzleTime(3000)
                    .question("What is my real name?")
                    .addChoice("Wilson Fisk", false)
                    .addChoice("Frank Castle", false)
                    .addChoice("Adrian Toomes", true)
                    .addChoice("Eddie Brock", false)
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Dr. Doom")
                .location(49.210672, 16.615755)
                .radius(10)
                .hint("drdoom")
                .imageSelectPuzzle()
                .question("Remember the order!!!")
                .hint("drdoom")
                .puzzleDone()
                .taskDone();

        builder.addGPSTask("Lizard")
                .location(49.211232, 16.617286)
                .radius(10)
                .victoryPoints(10)
                .hint("lizard")
                .simplePuzzle()
                .question("Who’s the biggest lizard of this world")
                .answer("zuckerberg")
                .hint("lizard")
                .puzzleDone()
                .taskDone();

        builder.addGPSTask("Baron")
                .location(49.210625, 16.617437)
                .radius(10)
                .victoryPoints(10)
                .hint("baron")
                .imageSelectPuzzle()
                .question("What glyph does doctor strange use?")
                .addImage(R.drawable.glyph, true)
                .addImage(R.drawable.glyph2, false)
                .addImage(R.drawable.glyph3, false)
                .hint("baron")
                .puzzleDone()
                .taskDone();

        builder.addGPSTask("Venom")
                .location(49.209951, 16.616727)
                .radius(10)
                .victoryPoints(10)
                .hint("venom")
                .choicePuzzle()
                .question("What is this alien species name?")
                .addChoice("Asogian", false)
                .addChoice("Symbiote", true)
                .addChoice("Unggoy", false)
                .addChoice("Irken", false)
                .hint("venom")
                .puzzleDone()
                .taskDone();

        builder.addGPSTask("Dormammu")
                .location(49.209787, 16.614962)
                .radius(30)
                .imageSelectPuzzle()
                .question("Remember the order!!!")
                .puzzleDone()
                .taskDone();


        builder.addGPSTask("End Boss")
                .location(49.209984, 16.614801)
                .radius(10)
                .victoryPoints(10)
                .hint("thanos")
                .simplePuzzle()
                .question("What are the stones in my gauntlet called? ")
                .answer("infinity stones")
                .hint("stones")
                .puzzleDone()
                .taskDone();

        builder.addBeaconTask("Thanomir")
                .location(49.209759, 16.614594)
                .beacon(46269,42519)
                .victoryPoints(10)
                .hint("thanos")
                .simplePuzzle()
                .question("What are the stones in my gauntlet called? ")
                .answer("infinity stones")
                .hint("stones")
                .puzzleDone()
                .taskDone();
    }

}
