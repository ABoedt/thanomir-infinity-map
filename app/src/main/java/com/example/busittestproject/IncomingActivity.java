package com.example.busittestproject;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.gigamole.library.PulseView;

import java.util.Timer;
import java.util.TimerTask;

public class IncomingActivity extends AppCompatActivity {

    private TextView incomingMsg;
    private PulseView warningSign;
    private static final int SCREEN_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming);

        incomingMsg = findViewById(R.id.incomingMsg);
        manageBlink();

        warningSign = findViewById(R.id.warningSign);
        warningSign.startPulse();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(IncomingActivity.this, EndBossActivity.class);
                startActivity(intent);
                finish();
            }
        }, SCREEN_TIME);
    }

    private void manageBlink() {
        ObjectAnimator animator = ObjectAnimator.ofInt(incomingMsg, "textColor", Color.WHITE, Color.BLACK);
        animator.setDuration(SCREEN_TIME);
        animator.setEvaluator(new ArgbEvaluator());
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.start();
    }
}
