package com.example.busittestproject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.mendelu.busItWeek.library.ChoicePuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;

public class ChoicePuzzleActivity extends AppCompatActivity {

    private TextView questionTextView;
    private ImageView hintImageView;
    private RecyclerView recyclerView;

    private StoryLine storyLine;
    private Task currentTask;
    private ChoicePuzzle puzzle;

    private AnswersAdapter answersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_puzzle);

        questionTextView = findViewById(R.id.question);
        hintImageView = findViewById(R.id.hint);
        recyclerView = findViewById(R.id.answers);

        storyLine = StoryLine.open(this, BusITWeekDatabaseHelper.class);
    }

    @Override
    public void onBackPressed() {
        skipCurrentTask();
    }

    private class AnswersAdapter extends RecyclerView.Adapter<AnswersAdapter.MyViewHolder>{

        private List<String> answersList;

        public AnswersAdapter(List<String> answersList) {
            this.answersList = answersList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.row_choice_puzzle, viewGroup, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int position) {
            String answer = answersList.get(position);
            myViewHolder.answer.setText(answer);
            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (puzzle.getAnswerForChoice(myViewHolder.getAdapterPosition())){
                        currentTask.finish(true);
                        finish();
                    }else{
                        Toast.makeText(ChoicePuzzleActivity.this, "Wrong answer", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return answersList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{

            public TextView answer;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                answer = itemView.findViewById(R.id.answer);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        currentTask = storyLine.currentTask();
        if (currentTask != null){
            setTitle(currentTask.getName());
            puzzle = (ChoicePuzzle) currentTask.getPuzzle();
            questionTextView.setText(puzzle.getQuestion());
            Picasso.get().load(ResourceUtility.getResourceDrawableID(this, puzzle.getHint())).transform(new RoundCornersTransformation(25, 5, true, true)).into(hintImageView);
            initializeList();
        }
    }

    private void initializeList(){
        List<String> lst = new ArrayList<>();

        for (Map.Entry<String,Boolean> entry : puzzle.getChoices().entrySet()){
            lst.add(entry.getKey());
        }

        answersAdapter = new AnswersAdapter(lst);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(answersAdapter);
    }



    private void skipCurrentTask(){
        DialogUtility.skipTask(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                storyLine.currentTask().skip();
                finish();
            }
        });
    }

}
