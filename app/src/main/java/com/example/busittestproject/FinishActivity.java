package com.example.busittestproject;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;
import cz.mendelu.busItWeek.library.TaskStatus;

public class FinishActivity extends AppCompatActivity {

    private TextView score;
    private StoryLine storyLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        score = findViewById(R.id.score);
        storyLine = StoryLine.open(this, BusITWeekDatabaseHelper.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle("Game Over");
        int totalScore = 0;

        for (Task task : storyLine.taskList()) {
            if (task.getTaskStatus() == TaskStatus.SUCCESS) {
                totalScore += task.getVictoryPoints();
            }
        }
        score.append(totalScore + "");
    }
}
