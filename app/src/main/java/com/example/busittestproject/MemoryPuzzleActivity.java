package com.example.busittestproject;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.busItWeek.library.ImageSelectPuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;

public class MemoryPuzzleActivity extends AppCompatActivity {

    private TextView questionTextView;
    private List<Integer> numberOrder;

    private StoryLine storyLine;
    private Task currentTask;
    private ImageSelectPuzzle puzzle;

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private ProgressBar enemyHealth;
    private TextView enemyName;
    private ImageView enemyImage;

    private int correctAnswerCounter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_puzzle);

        questionTextView = findViewById(R.id.question);

        correctAnswerCounter = 1;

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);

        enemyName = findViewById(R.id.miniBossName);
        enemyHealth = findViewById(R.id.miniBossHealth);
        enemyImage = findViewById(R.id.miniBossImage);

        storyLine = StoryLine.open(this, BusITWeekDatabaseHelper.class);

        setVillian();

        resetArray();

        setButtonText();

        startTimer();
    }

    private void setVillian() {
        if (storyLine.currentTask().getName().equals("Dr. Doom")) {
            enemyName.setText("Dr. Doom");
            enemyImage.setImageResource(R.drawable.drdoom);
        } else if (storyLine.currentTask().getName().equals("Dormammu")) {
            enemyName.setText("Dormammu");
            enemyImage.setImageResource(R.drawable.dormammu);
        }
    }

    private void resetArray() {
        numberOrder = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            getIntNotInArray(randomizeNumber());
        }
    }

    private int getIntNotInArray(int number) {
        int newNumber = number;

        if (numberOrder.size()==5) {
            numberOrder.add(6);
            return 6;
        }

        while (numberOrder.contains(newNumber)) {
            newNumber = ((newNumber + 1) % 5) + 1;
        }
        numberOrder.add(newNumber);
        return newNumber;
    }

    private int randomizeNumber() {
        return (int) (Math.random() * 5 + 1);
    }

    @Override
    public void onBackPressed() {
        //makes sure user can finish.
        //if super is deleted, user cannot go back.
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.getSupportActionBar().hide();
        currentTask = storyLine.currentTask();
        if (currentTask != null) {
            puzzle = (ImageSelectPuzzle) currentTask.getPuzzle();
            questionTextView.setText(puzzle.getQuestion());
        }
    }

    private void startTimer() {
        long timeLeft = 3000;
        CountDownTimer countDownTimer = new CountDownTimer(timeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                emptyButtonText();
            }
        }.start();
    }

    private void setButtonText() {
        questionTextView.setText("Remember!!!");
        button1.setText("" + numberOrder.get(0));
        button2.setText("" + numberOrder.get(1));
        button3.setText("" + numberOrder.get(2));
        button4.setText("" + numberOrder.get(3));
        button5.setText("" + numberOrder.get(4));
        button6.setText("" + numberOrder.get(5));

        button1.setBackgroundResource(R.drawable.circlered);
        button2.setBackgroundResource(R.drawable.circleblue);
        button3.setBackgroundResource(R.drawable.circlegreen);
        button4.setBackgroundResource(R.drawable.circleyellow);
        button5.setBackgroundResource(R.drawable.circlepurple);
        button6.setBackgroundResource(R.drawable.circleorange);

        button1.setOnClickListener(null);
        button2.setOnClickListener(null);
        button3.setOnClickListener(null);
        button4.setOnClickListener(null);
        button5.setOnClickListener(null);
        button6.setOnClickListener(null);
    }

    private void emptyButtonText() {
        questionTextView.setText("What was the order?");
        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        button5.setText("");
        button6.setText("");
        setOnclicks();
    }

    private void setOnclicks() {
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button1.setBackgroundResource(R.drawable.circledarkred);
                checkAnswers(0);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button2.setBackgroundResource(R.drawable.circledarkblue);
                checkAnswers(1);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button3.setBackgroundResource(R.drawable.circledarkgreen);
                checkAnswers(2);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button4.setBackgroundResource(R.drawable.circledarkyellow);
                checkAnswers(3);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button5.setBackgroundResource(R.drawable.circledarkpurple);
                checkAnswers(4);
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button6.setBackgroundResource(R.drawable.circledarkorange);
                checkAnswers(5);
            }
        });
    }

    private void checkAnswers(int listIndex) {
        if (numberOrder.get(listIndex)==correctAnswerCounter) {
            correctAnswerCounter++;
        } else {
            Toast.makeText(MemoryPuzzleActivity.this, "Wrong order, restart", Toast.LENGTH_SHORT).show();
            correctAnswerCounter = 1;
            setButtonText();
            startTimer();
        }

        if (correctAnswerCounter>6) {
            enemyHealth.setProgress(enemyHealth.getProgress()-34, true);
            resetArray();
            correctAnswerCounter = 1;
            setButtonText();
            startTimer();
        }

        if (enemyHealth.getProgress()<=0) {
            storyLine.currentTask().finish(true);
            finish();
        }
    }
}
