package com.example.busittestproject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import cz.mendelu.busItWeek.library.SimplePuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;

public class SimplePuzzleActivity extends AppCompatActivity {

    private TextView questionTextView;
    private EditText answerEditText;
    private Button doneButton;
    private ImageView hintImageView;

    private StoryLine storyLine;
    private Task currentTask;
    private SimplePuzzle puzzle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_puzzle);

        questionTextView = findViewById(R.id.question);
        answerEditText = findViewById(R.id.answer);
        doneButton = findViewById(R.id.done);
        hintImageView = findViewById(R.id.hint);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer();
            }
        });

        storyLine = StoryLine.open(this, BusITWeekDatabaseHelper.class);
    }

    @Override
    public void onBackPressed() {
        skipCurrentTask();
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentTask = storyLine.currentTask();
        if (currentTask != null){
            setTitle(currentTask.getName());
            puzzle = (SimplePuzzle) currentTask.getPuzzle();
            questionTextView.setText(puzzle.getQuestion());
            Picasso.get().load(ResourceUtility.getResourceDrawableID(this, puzzle.getHint())).transform(new RoundCornersTransformation(25, 5, true, true)).into(hintImageView);
            answerEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }
    }

    private void checkAnswer(){
        String userAnswer = answerEditText.getText().toString().toLowerCase();
        String correctAnswer = puzzle.getAnswer();

        if(userAnswer.contains(puzzle.getAnswer())){
            storyLine.currentTask().finish(true);
            finish();
        }else{
            Toast.makeText(SimplePuzzleActivity.this, "Wrong answer", Toast.LENGTH_SHORT).show();
            storyLine.currentTask().finish(false);
            finish();
        }
    }

    private void skipCurrentTask(){
        DialogUtility.skipTask(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                storyLine.currentTask().skip();
                finish();
            }
        });
    }

}
