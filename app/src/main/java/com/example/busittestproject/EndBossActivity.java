package com.example.busittestproject;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;

public class EndBossActivity extends AppCompatActivity {

    private List<ProgressBar> healthBars;
    private ImageView thanos;

    private Button attack;

    private int currentBar;

    private TextView timer;
    private TextView defeat;

    private CountDownTimer countDownTimer;
    private long timeLeft = 61000;

    private StoryLine storyLine;
    private Task currentTask;

    private ProgressBar health3;

    private LinearLayout simpleQuestionLayout;
    private LinearLayout choiceQuestionLayout;

    private TextView simpleQuestion;
    private TextView choiceQuestion;
    private RecyclerView recyclerView;
    private EditText answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_boss);

        storyLine = StoryLine.open(this, BusITWeekDatabaseHelper.class);

        timer = findViewById(R.id.timer);
        defeat = findViewById(R.id.defeat);
        ProgressBar health1 = findViewById(R.id.health_1);
        ProgressBar health2 = findViewById(R.id.health_2);
        health3 = findViewById(R.id.health_3);
        attack = findViewById(R.id.attack);
        simpleQuestion = findViewById(R.id.simple_question);
        choiceQuestion = findViewById(R.id.choice_question);
        recyclerView = findViewById(R.id.answers);
        answer = findViewById(R.id.answer);
        simpleQuestionLayout = findViewById(R.id.simple_layout);
        choiceQuestionLayout = findViewById(R.id.choice_layout);
        thanos = findViewById(R.id.boss);

        startTimer();

        healthBars = new ArrayList<>();
        currentBar = 0;

        healthBars.add(health1);
        healthBars.add(health2);
        healthBars.add(health3);
        if (EndBoss.isSecondChance()) {
            setSecondQuestions();
        }

    }


    private void setSecondQuestions() {
        simpleQuestion.setText(getString(R.string.question2));
        TextView answers1 = findViewById(R.id.answer1);
        TextView answers2 = findViewById(R.id.answer2);
        TextView answers3 = findViewById(R.id.answer3);
        choiceQuestion.setText("Who is my greatest threat?");
        answers1.setText("Groot");
        answers2.setText("Thor");
        answers3.setText("Iron Man");
    }

    public void attack(View view) {
        ProgressBar current = healthBars.get(currentBar);

        current.setProgress(current.getProgress() - 1, true);

        if (current.getProgress() == 34) {
            changeToQuestion();
        }

        if (current.getProgress() == 0 && currentBar < 2) {
            changeHealthBar(current);
        }
    }

    private void changeHealthBar(ProgressBar current) {
        currentBar++;
        current.setVisibility(View.GONE);
        current = healthBars.get(currentBar);
        current.setVisibility(View.VISIBLE);

        if (currentBar == 1) {
            thanos.setImageResource(R.drawable.thanoshurt);
        } else {
            thanos.setImageResource(R.drawable.thanoscry);
        }
    }

    @SuppressLint("SetTextI18n")
    private void changeToQuestion() {
        attack.setVisibility(View.GONE);
        if (currentBar == 0) {
            simpleQuestionLayout.setVisibility(View.VISIBLE);
        }
        if (currentBar == 1) {
            switchOnClickListenerState(false);

            startChoiceTimer();
            choiceQuestionLayout.setVisibility(View.VISIBLE);
        }

        if (currentBar == 2) {
            if (!EndBoss.isSecondChance()) {
                simpleQuestion.setText("What are the stones in my gauntlet called? ");
            } else {
                simpleQuestion.setText("What’s the best mobile OS?");
            }

            simpleQuestionLayout.setVisibility(View.VISIBLE);
        }

    }

    private void switchOnClickListenerState(boolean state) {
        TextView answer1 = findViewById(R.id.answer1);
        TextView answer2 = findViewById(R.id.answer2);
        TextView answer3 = findViewById(R.id.answer3);
        answer1.setClickable(state);
        answer2.setClickable(state);
        answer3.setClickable(state);
    }


    @Override
    protected void onResume() {
        super.onResume();
        currentTask = storyLine.currentTask();
        if (EndBoss.isDead()) {
            currentTask.finish(true);
            finish();
        }
        this.getSupportActionBar().hide();
    }

    private void startChoiceTimer() {
        CountDownTimer countDownTimer2 = new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                switchOnClickListenerState(true);
            }
        }.start();
    }


    private void startTimer(){
        countDownTimer = new CountDownTimer(timeLeft,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                updateTimer();
            }

            @Override
            public void onFinish() {
                Toast.makeText(EndBossActivity.this, "Thanomir ran away", Toast.LENGTH_SHORT).show();
                EndBoss.setSecondChance(true);
                timer.setVisibility(View.GONE);
                defeat.setVisibility(View.VISIBLE);
                currentTask.finish(false);
                finish();
            }
        }.start();
    }

    private void updateTimer() {
        int minutes = (int) timeLeft / 60000;
        int seconds = (int) timeLeft % 60000 / 1000;

        String timeString = String.valueOf(minutes) + ":";
        if(seconds < 10){
            timeString += "0";
        }
        timeString += seconds;

        timer.setText(timeString);

        if (health3.getProgress() <= 0) {
            EndBoss.bossDied();
            currentTask.finish(true);
            finish();
        }
    }


    public void checkAnswer(View view) {
        if (currentBar == 0) {
            if (!EndBoss.isSecondChance()) {
                compare("avengers");
            } else {
                compare("wakanda");
            }

        }

        if (currentBar == 2) {
            if (!EndBoss.isSecondChance()) {
                compare("infinity stones");
            } else {
                compare("android");
            }
        }
    }

    private void compare(String toCompare) {
        ProgressBar current = healthBars.get(currentBar);
        if (answer.getText().toString().toLowerCase().contains(toCompare)){
            current.setProgress(current.getProgress() - 32, true);
            changeToButton();
        }else {
            changeToButton();
        }
    }


    private void changeToButton() {
        if (currentBar == 0 || currentBar == 2) {
            simpleQuestionLayout.setVisibility(View.GONE);
            attack.setVisibility(View.VISIBLE);
            answer.setText("");
        } else {
            choiceQuestionLayout.setVisibility(View.GONE);
            attack.setVisibility(View.VISIBLE);
        }
    }

    public void wrong(View view) {
        changeToButton();
    }

    public void correct(View view) {
        ProgressBar current = healthBars.get(currentBar);

        current.setProgress(current.getProgress() - 32, true);
        changeToButton();
    }
}
